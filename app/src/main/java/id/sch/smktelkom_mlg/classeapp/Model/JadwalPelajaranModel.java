package id.sch.smktelkom_mlg.classeapp.Model;

/**
 * Created by Haidar on 29/04/2018.
 */

public class JadwalPelajaranModel {
    String id, hari, mapel_1, mapel_2, mapel_3, mapel_4, mapel_5;

    public JadwalPelajaranModel(String id, String hari, String mapel_1, String mapel_2, String mapel_3, String mapel_4, String mapel_5) {
        this.id = id;
        this.hari = hari;
        this.mapel_1 = mapel_1;
        this.mapel_2 = mapel_2;
        this.mapel_3 = mapel_3;
        this.mapel_4 = mapel_4;
        this.mapel_5 = mapel_5;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getMapel_1() {
        return mapel_1;
    }

    public void setMapel_1(String mapel_1) {
        this.mapel_1 = mapel_1;
    }

    public String getMapel_2() {
        return mapel_2;
    }

    public void setMapel_2(String mapel_2) {
        this.mapel_2 = mapel_2;
    }

    public String getMapel_3() {
        return mapel_3;
    }

    public void setMapel_3(String mapel_3) {
        this.mapel_3 = mapel_3;
    }

    public String getMapel_4() {
        return mapel_4;
    }

    public void setMapel_4(String mapel_4) {
        this.mapel_4 = mapel_4;
    }

    public String getMapel_5() {
        return mapel_5;
    }

    public void setMapel_5(String mapel_5) {
        this.mapel_5 = mapel_5;
    }
}
