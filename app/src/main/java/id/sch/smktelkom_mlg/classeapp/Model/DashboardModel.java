package id.sch.smktelkom_mlg.classeapp.Model;

public class DashboardModel {
    String id, nama, foto_profil, deskripsi;

    public DashboardModel() {
    }

    public DashboardModel(String id, String nama, String foto_profil, String deskripsi) {
        this.id = id;
        this.nama = nama;
        this.foto_profil = foto_profil;
        this.deskripsi = deskripsi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getFoto_profil() {
        return foto_profil;
    }

    public void setFoto_profil(String foto_profil) {
        this.foto_profil = foto_profil;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
