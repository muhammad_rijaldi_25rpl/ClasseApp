package id.sch.smktelkom_mlg.classeapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.sch.smktelkom_mlg.classeapp.Model.DashboardModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class Dashboard extends Fragment {
    RequestQueue queue;
    ImageView ivFotoProfil;
    TextView tvNama, tvDesc;
    List<DashboardModel> dashboardClassList = new ArrayList<>();
    String url = "http://192.168.43.29/rest_server/index.php/dashboard";
    ProgressBar pg;
    public Dashboard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ivFotoProfil = rootView.findViewById(R.id.ivFotoProfil);
        tvNama = rootView.findViewById(R.id.tvNama);
        tvDesc = rootView.findViewById(R.id.tvDesc);
        queue = Volley.newRequestQueue(getContext());
        pg = rootView.findViewById(R.id.pgBar);
        pg.setVisibility(View.VISIBLE);
        getVolley();

        return rootView;
    }

    private void getVolley() {
        dashboardClassList.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                getJSON(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void getJSON(String request) {
        try {
            JSONArray jsonArray = new JSONArray(request);
            JSONObject jObject = jsonArray.getJSONObject(0);
            DashboardModel db = new DashboardModel(
                    jObject.getString("id"),
                    jObject.getString("nama"),
                    jObject.getString("foto_profil"),
                    jObject.getString("deskripsi"));
            tvNama.setText(db.getNama());
            tvDesc.setText(db.getDeskripsi());
            Glide.with(this)
                    .load(db.getFoto_profil())
                    .into(ivFotoProfil);
            pg.setVisibility(View.GONE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
