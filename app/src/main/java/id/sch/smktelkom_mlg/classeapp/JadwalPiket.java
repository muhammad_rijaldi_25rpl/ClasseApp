package id.sch.smktelkom_mlg.classeapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.sch.smktelkom_mlg.classeapp.Adapter.JadwalPiketAdapter;
import id.sch.smktelkom_mlg.classeapp.Model.JadwalPiketModel;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link JadwalPiket.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JadwalPiket#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JadwalPiket extends Fragment {
    String url = "http://192.168.43.29/rest_server/index.php/jadwal_piket";
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    List<JadwalPiketModel> piketClassList = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressBar pg;
    private OnFragmentInteractionListener mListener;

    public JadwalPiket() {
        // Required empty public constructor
    }

    public static JadwalPiket newInstance(String param1, String param2) {
        JadwalPiket fragment = new JadwalPiket();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_jadwal_piket, container, false);
        recyclerView = rootView.findViewById(R.id.recyclerview_jadwal_piket);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout = rootView.findViewById(R.id.contentView);
        pg = rootView.findViewById(R.id.pgBar);
        pg.setVisibility(View.VISIBLE);
        getvolley();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getvolley();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getvolley() {
        piketClassList.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                getJSON(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    public void getJSON(String request) {
        try {

            JSONArray jsonArray = new JSONArray(request);
            for (int x = 0; x < jsonArray.length(); x++) {
                JSONObject jObject = jsonArray.getJSONObject(x);
                JadwalPiketModel piket = new JadwalPiketModel(
                        jObject.getString("id"),
                        jObject.getString("hari"),
                        jObject.getString("nama_1"),
                        jObject.getString("nama_2"),
                        jObject.getString("nama_3"),
                        jObject.getString("nama_4"),
                        jObject.getString("nama_5"));
                piketClassList.add(piket);
                adapter = new JadwalPiketAdapter(piketClassList);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(adapter);
                pg.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
