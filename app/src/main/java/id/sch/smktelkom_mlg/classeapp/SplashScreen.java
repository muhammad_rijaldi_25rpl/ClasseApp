package id.sch.smktelkom_mlg.classeapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends AppCompatActivity {

    ImageView iv, iv_delay;
    Animation anim_text, anim_delay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        iv = findViewById(R.id.image_text);
        iv_delay = findViewById(R.id.final_delay);

        anim_text = AnimationUtils.loadAnimation(this, R.anim.anim_text);
        anim_delay = AnimationUtils.loadAnimation(this, R.anim.final_delay);

        anim_text.setStartTime(Animation.START_ON_FIRST_FRAME);
        iv.startAnimation(anim_text);

        anim_delay.setStartTime(0);
        iv_delay.startAnimation(anim_delay);

        anim_delay.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startActivity(new Intent(getApplicationContext(), WalkthorughActivity.class));
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
