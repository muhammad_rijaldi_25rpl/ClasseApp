package id.sch.smktelkom_mlg.classeapp.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.sch.smktelkom_mlg.classeapp.Model.JadwalPelajaranModel;
import id.sch.smktelkom_mlg.classeapp.R;

/**
 * Created by Haidar on 30/04/2018.
 */

public class JadwalPelajaranAdapter extends RecyclerView.Adapter<JadwalPelajaranAdapter.Holder> {
    List<JadwalPelajaranModel> pelajaranClassList;

    public JadwalPelajaranAdapter(List<JadwalPelajaranModel> halal) {
        pelajaranClassList = halal;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_jadwalpelajaran_data, null);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        JadwalPelajaranModel h = pelajaranClassList.get(position);
        holder.tvHari.setText(h.getHari());
        holder.tvMapel1.setText(h.getMapel_1());
        holder.tvMapel2.setText(h.getMapel_2());
        holder.tvMapel3.setText(h.getMapel_3());
        holder.tvMapel4.setText(h.getMapel_4());
        holder.tvMapel5.setText(h.getMapel_5());
    }

    @Override
    public int getItemCount() {
        return pelajaranClassList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tvHari, tvMapel1, tvMapel2, tvMapel3, tvMapel4, tvMapel5;

        public Holder(View itemView) {
            super(itemView);
            tvHari = itemView.findViewById(R.id.textViewHari);
            tvMapel1 = itemView.findViewById(R.id.textViewMapel1);
            tvMapel2 = itemView.findViewById(R.id.textViewMapel2);
            tvMapel3 = itemView.findViewById(R.id.textViewMapel3);
            tvMapel4 = itemView.findViewById(R.id.textViewMapel4);
            tvMapel5 = itemView.findViewById(R.id.textViewMapel5);
        }
    }
}