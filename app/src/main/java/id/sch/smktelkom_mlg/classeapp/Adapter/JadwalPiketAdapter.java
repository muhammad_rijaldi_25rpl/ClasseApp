package id.sch.smktelkom_mlg.classeapp.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.sch.smktelkom_mlg.classeapp.Model.JadwalPiketModel;
import id.sch.smktelkom_mlg.classeapp.R;

/**
 * Created by Haidar on 30/04/2018.
 */

public class JadwalPiketAdapter extends RecyclerView.Adapter<JadwalPiketAdapter.Holder> {
    List<JadwalPiketModel> piketClassList;

    public JadwalPiketAdapter(List<JadwalPiketModel> halal) {
        piketClassList = halal;
    }


    @Override
    public JadwalPiketAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_jadwalpiket_data, null);
        return new JadwalPiketAdapter.Holder(v);
    }

    @Override
    public void onBindViewHolder(JadwalPiketAdapter.Holder holder, int position) {
        JadwalPiketModel h = piketClassList.get(position);
        holder.tvHari.setText(h.getHari());
        holder.tvNama1.setText(h.getNama_1());
        holder.tvNama2.setText(h.getNama_2());
        holder.tvNama3.setText(h.getNama_3());
        holder.tvNama4.setText(h.getNama_4());
        holder.tvNama5.setText(h.getNama_5());
    }

    @Override
    public int getItemCount() {
        return piketClassList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tvHari, tvNama1, tvNama2, tvNama3, tvNama4, tvNama5;

        public Holder(View itemView) {
            super(itemView);
            tvHari = itemView.findViewById(R.id.textViewHari);
            tvNama1 = itemView.findViewById(R.id.textViewNama1);
            tvNama2 = itemView.findViewById(R.id.textViewNama2);
            tvNama3 = itemView.findViewById(R.id.textViewNama3);
            tvNama4 = itemView.findViewById(R.id.textViewNama4);
            tvNama5 = itemView.findViewById(R.id.textViewNama5);
        }
    }
}