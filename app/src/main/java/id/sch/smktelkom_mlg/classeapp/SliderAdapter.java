package id.sch.smktelkom_mlg.classeapp;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Root on 3/14/2018.
 */

public class SliderAdapter extends PagerAdapter {

    //Arrays
    public int[] slide_images = {
            R.drawable.absn,
            R.drawable.kas,
            R.drawable.duduk,
            R.drawable.rangkuman,
            R.drawable.piket,
            R.drawable.jdwl,
    };
    public String[] slide_headings = {
            "Absensi",
            "Data Kas",
            "Tempat Duduk",
            "Rangkuman",
            "Jadwal Piket",
            "Jadwal Pelajaran",

    };
    public String[] slide_descs = {
            "Kamu dapat mengetahui anggota kelas yang tidak masuk pada hari itu.",
            "Kamu bisa mengetahui jumlah kas yang belum dan sudah di bayar.",
            "Mengetahui perubahan posisi tempat duduk anda saat itu.",
            "Dengan adanya rangkuman, tugas dan catatan mu akan semakin teratur.",
            "Buat kelasmu tetap bersih dan nyaman dengan mengetahui jadwal piket.",
            "Jadwal pelajaran akan membuat kamu tidak ketinggalan pelajaran.",
    };
    Context context;
    LayoutInflater LayoutInflater;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView slideImageView = view.findViewById(R.id.slide_image);
        TextView slideHeading = view.findViewById(R.id.slide_heading);
        TextView slideDescription = view.findViewById(R.id.slide_desc);

        slideImageView.setImageResource(slide_images[position]);
        slideHeading.setText(slide_headings[position]);
        slideDescription.setText(slide_descs[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((RelativeLayout) object);
    }
}
