package id.sch.smktelkom_mlg.classeapp.Model;

/**
 * Created by Haidar on 30/04/2018.
 */

public class JadwalPiketModel {
    String id, hari, nama_1, nama_2, nama_3, nama_4, nama_5;

    public JadwalPiketModel(String id, String hari, String nama_1, String nama_2, String nama_3, String nama_4, String nama_5) {
        this.id = id;
        this.hari = hari;
        this.nama_1 = nama_1;
        this.nama_2 = nama_2;
        this.nama_3 = nama_3;
        this.nama_4 = nama_4;
        this.nama_5 = nama_5;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getNama_1() {
        return nama_1;
    }

    public void setNama_1(String nama_1) {
        this.nama_1 = nama_1;
    }

    public String getNama_2() {
        return nama_2;
    }

    public void setNama_2(String nama_2) {
        this.nama_2 = nama_2;
    }

    public String getNama_3() {
        return nama_3;
    }

    public void setNama_3(String nama_3) {
        this.nama_3 = nama_3;
    }

    public String getNama_4() {
        return nama_4;
    }

    public void setNama_4(String nama_4) {
        this.nama_4 = nama_4;
    }

    public String getNama_5() {
        return nama_5;
    }

    public void setNama_5(String nama_5) {
        this.nama_5 = nama_5;
    }
}
